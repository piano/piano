'''synth.py

A simple wrapper around fluidsynth openrc service.
'''
#
# Copyright 2021 lafleur
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import socket
from subprocess import call, Popen, PIPE


class SynthClient():
    '''Thin wrapper around fluidsynth. Connecting via TCP.

    Note that we expect fluidsynth to be an openrc service, so the
    config_load() and config_save() functions are not used.
    '''

    fluidsynth_soundfont_index = 1

    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if self.started():
            self.socket.connect(('localhost', 9800))

    def started(self):
        '''Tell if the fluidsynth openrc service is started'''
        test = Popen(['service', 'fluidsynth', 'status'], stdout=PIPE)
        test.communicate()
        if test.returncode == 0:
            return True
        return False

    def start(self):
        '''Start the fluidsynth openrc service and enable it'''
        if not self.started():
            print('starting fluidsynth')
            call(['sudo', 'rc-update',  'add', 'fluidsynth'])
            call(['sudo', 'service', 'fluidsynth', 'start'])
            self.socket.connect(('localhost', 9800))

    def stop(self):
        '''Stop the fluidsynth openrc service and disable it'''
        if self.started():
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
            print('stopping fluidsynth')
            call(['sudo', 'service', 'fluidsynth', 'stop'], stdout=PIPE)
            call(['sudo', 'rc-update',  'del', 'fluidsynth'], stdout=PIPE)
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def quit(self):
        if self.started():
            self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

    def tell(self, command):
        '''Send a command to the fluidsynth daemon'''
        self.socket.send(command.encode() + b'\n')
        # Flush the stdout buffer from the command :
        #self.process.stdout.readline()

    def ask(self, question):
        '''Send a question to the fluidsynth daemon and return the answer'''
        self.socket.send(question.encode() + b'\n')
        return self._readline()

    def load_soundfont(self, font):
        '''Replace the current soundfont by another one'''
        self.tell("unload " + str(self.fluidsynth_soundfont_index))
        self.fluidsynth_soundfont_index += 1
        self.tell("load " + font)

    def ask_soundfont_name(self):
        '''Return the upmost font loaded

        The "fonts" command in fluidsynth returns a list with indexed font
        names, and a header. The "ask()" method returns the header ; then we
        take the next line's last word and return it.
        '''
        self.ask("fonts")
        return self._readline().split(' ')[-1]

    def _readline(self):
        payload = b''
        for block in iter(lambda: self.socket.recv(1), b'\n'):
            payload += block
        return payload.decode()

