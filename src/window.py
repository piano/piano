'''window.py

Methods for the piano application.
'''
#
# Copyright 2021 lafleur
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from subprocess import Popen
from gi.repository import Gtk, Handy

from .synth import SynthClient


@Gtk.Template(resource_path='/io/frama/Piano/window.ui')
class PianoWindow(Gtk.ApplicationWindow):
    '''A Gtk+libhandy fluidsynth GUI.
    '''
    __gtype_name__ = 'PianoWindow'

    Handy.init()
    squeezer = Gtk.Template.Child()
    headerbar_switcher = Gtk.Template.Child()
    bottom_switcher = Gtk.Template.Child()
    main_toggle = Gtk.Template.Child()
    gain = Gtk.Template.Child()
    reverb_toggle = Gtk.Template.Child()
    reverb_level = Gtk.Template.Child()
    reverb_damp = Gtk.Template.Child()
    reverb_room_size = Gtk.Template.Child()
    reverb_width = Gtk.Template.Child()
    chorus_toggle = Gtk.Template.Child()
    chorus_level = Gtk.Template.Child()
    chorus_depth = Gtk.Template.Child()
    chorus_speed = Gtk.Template.Child()
    chorus_voice_count = Gtk.Template.Child()
    synth_main_box = Gtk.Template.Child()
    synth_off_box = Gtk.Template.Child()
    synth_on_box = Gtk.Template.Child()
    soundfont_chooser = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.squeezer.connect("notify::visible-child",
                              self.on_headerbar_squeezer_notify)
        self.connect("delete-event", self.on_quit)
        self.synth = SynthClient()
        if self.synth.started():
            self.update_gui(True)
            # We set main_toggle active before connecting it because the
            # daemon is already started, we don't mean to trigger it again.
            self.main_toggle.set_active(True)
        self.main_toggle.connect("notify::active", self.on_main_toggle)

    def update_gui(self, started):
        if started:
            self.synth_main_box.remove(self.synth_off_box)
            self.synth_main_box.add(self.synth_on_box)
            self.soundfont_chooser.set_filename(
                    self.synth.ask_soundfont_name())
            self.gain.set_value(
                    float(self.synth.ask('get synth.gain')))
            self.gain.connect("value-changed", self.on_gain)
            self.reverb_toggle.set_active(
                    int(self.synth.ask('get synth.reverb.active')))
            self.reverb_toggle.connect("notify::active",
                    self.on_reverb_toggle)
            self.reverb_level.set_value(
                    float(self.synth.ask('get synth.reverb.level')))
            self.reverb_level.connect("value-changed", self.on_reverb_level)
            self.reverb_damp.set_value(
                    float(self.synth.ask('get synth.reverb.damp')))
            self.reverb_damp.connect("value-changed", self.on_reverb_damp)
            self.reverb_room_size.set_value(
                    float(self.synth.ask('get synth.reverb.room-size')))
            self.reverb_room_size.connect("value-changed",
                    self.on_reverb_room_size)
            self.reverb_width.set_value(
                    float(self.synth.ask('get synth.reverb.width')))
            self.reverb_width.connect("value-changed", self.on_reverb_width)

            self.chorus_toggle.set_active(
                    int(self.synth.ask('get synth.chorus.active')))
            self.chorus_toggle.connect("notify::active",
                    self.on_chorus_toggle)
            self.chorus_level.set_value(
                    float(self.synth.ask('get synth.chorus.level')))
            self.chorus_level.connect("value-changed", self.on_chorus_level)
            self.chorus_depth.set_value(
                    float(self.synth.ask('get synth.chorus.depth')))
            self.chorus_depth.connect("value-changed", self.on_chorus_depth)
            self.chorus_speed.set_value(
                    float(self.synth.ask('get synth.chorus.speed')))
            self.chorus_speed.connect("value-changed", self.on_chorus_speed)
            self.chorus_voice_count.set_value(
                    float(self.synth.ask('get synth.chorus.nr')))
            self.chorus_voice_count.connect("value-changed",
                    self.on_chorus_voice_count)
        else:
            self.synth_main_box.remove(self.synth_on_box)
            self.synth_main_box.add(self.synth_off_box)
            # TODO Disable reverb and chorus.

    def on_main_toggle(self, toggle, event):
        if toggle.props.active:
            self.synth.start()
            self.update_gui(True)
        else:
            self.synth.stop()
            self.update_gui(False)

    def on_quit(self, window, event):
        self.synth.quit()

    def on_soundfont_choice(self, chooser):
        '''Load a soundfont, replacing the current one.'''
        # Even if we unload a font first, fluidsynth increments the font
        # index value, so we'll have to unload i+1 next time :
        self.synth.load_soundfont(chooser.get_filename())

    def on_gain(self, scroll):
        new_value = round(self.gain.get_value(), 3)
        self.gain.set_value(new_value)
        self.synth.tell('set synth.gain ' + str(new_value))
        print("gain changed to",
                self.synth.ask('get synth.gain'))

    def on_reverb_toggle(self, toggle, _):
        '''Toggle reverberation on or off.'''
        if toggle.props.active:
            self.synth.tell('set synth.reverb.active 1')
        else:
            self.synth.tell('set synth.reverb.active 0')
        print("reverb:", self.synth.ask('get synth.reverb.active'))

    def on_reverb_level(self, scroll):
        new_value = round(self.reverb_level.get_value(), 3)
        self.reverb_level.set_value(new_value)
        self.synth.tell('set synth.reverb.level ' + str(new_value))
        print("reverb level changed to",
                self.synth.ask('get synth.reverb.level'))

    def on_reverb_damp(self, scroll):
        new_value = round(self.reverb_damp.get_value(), 3)
        self.reverb_damp.set_value(new_value)
        self.synth.tell('set synth.reverb.damp ' + str(new_value))
        print("reverb damp changed to",
                self.synth.ask('get synth.reverb.damp'))

    def on_reverb_room_size(self, scroll):
        new_value = round(self.reverb_room_size.get_value(), 3)
        self.reverb_room_size.set_value(new_value)
        self.synth.tell('set synth.reverb.room-size ' + str(new_value))
        print("reverb room-size changed to",
                self.synth.ask('get synth.reverb.room-size'))

    def on_reverb_width(self, scroll):
        new_value = round(self.reverb_width.get_value(), 3)
        self.reverb_width.set_value(new_value)
        self.synth.tell('set synth.reverb.width ' + str(new_value))
        print("reverb width changed to",
                self.synth.ask('get synth.reverb.width'))

    def on_chorus_toggle(self, toggle, _):
        '''Toggle chorus on or off.'''
        if toggle.props.active:
            self.synth.tell('set synth.chorus.active 1')
        else:
            self.synth.tell('set synth.chorus.active 0')
        print("chorus:", self.synth.ask('get synth.chorus.active'))

    def on_chorus_level(self, scroll):
        new_value = round(self.chorus_level.get_value(), 3)
        self.chorus_level.set_value(new_value)
        self.synth.tell('set synth.chorus.level ' + str(new_value))
        print("chorus level changed to",
                self.synth.ask('get synth.chorus.level'))

    def on_chorus_depth(self, scroll):
        new_value = round(self.chorus_depth.get_value(), 3)
        self.chorus_depth.set_value(new_value)
        self.synth.tell('set synth.chorus.depth ' + str(new_value))
        print("chorus depth changed to",
                self.synth.ask('get synth.chorus.depth'))

    def on_chorus_speed(self, scroll):
        new_value = round(self.chorus_speed.get_value(), 3)
        self.chorus_speed.set_value(new_value)
        self.synth.tell('set synth.chorus.speed ' + str(new_value))
        print("chorus speed changed to",
                self.synth.ask('get synth.chorus.speed'))

    def on_chorus_voice_count(self, scroll):
        new_value = round(self.chorus_voice_count.get_value(), 3)
        self.chorus_voice_count.set_value(new_value)
        self.synth.tell('set synth.chorus.nr ' + str(new_value))
        print("chorus voice_count changed to",
                self.synth.ask('get synth.chorus.nr'))

    def on_headerbar_squeezer_notify(self, squeezer, _):
        '''Squeeze top buttons to bottom if the upper side is full.'''
        child = squeezer.get_visible_child()
        self.bottom_switcher.set_reveal(child != self.headerbar_switcher)

